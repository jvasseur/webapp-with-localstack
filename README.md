# webapp-with-localstack

A sample webapp using localstack services (Lambda, SQS, DynamoDB).

This is a proof of concept for:

* running a web app with its own docker-compose
* running localstack with services that can be called from the webapp

The flow that we want to achieve here is:

* webapp sends a message to the SQS
* SQS triggers a lambda function
* lambda function records an entry to DynamoDB
* webapp displays DynamoDB contents

## Prerequisites

* `aws-cli`

## Start localstack

`bash ./scripts/start_localstack.sh`

## Start webapp

*TODO*

## Useful commands

List Lambda event source mappings:
`aws --endpoint-url=http://localhost:4574 lambda list-event-source-mappings`

Invoke Lambda function:
`aws --endpoint-url http://localhost:4574 lambda invoke --function-name my_lambda outfile.txt`

Send a message to SQS:
`aws --endpoint-url=http://localhost:4576 sqs send-message --queue-url "http://localhost:4576/queue/my_sqs" --message-body '{}'`

Attach Lambda docker container to localstack network:
`docker network connect localstack_default localstack_lambda_arn_aws_lambda_us-east-1_000000000000_function_my_lambda --link localstack`

## Known issues

* Sending a message to SQS does not trigger the Lambda function: https://github.com/localstack/localstack/issues/967
